﻿using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

public class main : MonoBehaviour
{
   
    [SerializeField] TilemapManager tilemapManager;
    [SerializeField] public Button NextButton;
    [SerializeField] public Button RepeatButton;
    public State startState;
    public List<Position> possibaleMoves;
    UserLogic UserLogic;
    DFSLogic DFSLogic;
    BFSLogic BFSLogic;
    UCSLogic UCSLogic;
    AStarLogic AStarLogic;
    public static int Moves;
    public static int VisitedStates;
    public static long Time;
    State finalState;
    public Stopwatch st;
    
    private void Awake()
    {
        startState = new State((int[,])MainMenu.mainMenuManger.StartState.Clone(), MainMenu.mainMenuManger.StartPostion, MainMenu.mainMenuManger.FinalPosition);
    }

    
    private void Start()
    {
        int choice = 0;
        choice = PlayerPrefs.GetInt("choice");
       

        tilemapManager.create((int[,])startState.StateArray);

        //DFS
        DFSLogic = gameObject.AddComponent<DFSLogic>();
        DFSLogic.StartState = startState;

        //BFS
        BFSLogic = gameObject.AddComponent<BFSLogic>();
        BFSLogic.StartState = startState;

        //UCS
        UCSLogic = gameObject.AddComponent<UCSLogic>();
        UCSLogic.StartState = startState;

        //A*
        AStarLogic=gameObject.AddComponent<AStarLogic>();
        AStarLogic.StartState = startState;


        switch (choice)
        {
            case 1:
                PlayerInput();
                break;

            case 2:
                DFS();
                break;

            case 3:
                BFS();
                break;

            case 4:
                UCS();
                break;

            case 5:
                AStar();
                break;

            case 6:
                AStar2();
                break;
        }
    }
    public void PlayerInput()
    {
        //Play the game
        UserLogic = gameObject.AddComponent<UserLogic>();
        UserLogic.TilemapManager = tilemapManager;
        UserLogic.S1 = startState;
        UserLogic.Next = NextButton;
        UserLogic.Repeat = RepeatButton;
    }
    public void DFS()
    {
        Debug.Log("DFS started...");
        Stopwatch st = new Stopwatch();
        st.Start();
        State finalState = DFSLogic.DFS();
        st.Stop();

        if (finalState != null)
            tilemapManager.printPath(finalState);

        Time = st.ElapsedMilliseconds;
        VisitedStates = DFSLogic.visited.Count;
        Moves = tilemapManager.Moves;

        NextButton.gameObject.SetActive(true);
        RepeatButton.gameObject.SetActive(true);
    }

    public void BFS()
    {
        Debug.Log("BFS started...");
        Stopwatch st = new Stopwatch();
        st.Start();
        finalState = BFSLogic.BFS();
        st.Stop();

        if (finalState != null)
            tilemapManager.printPath(finalState);

        //Moving the status to the final Screne
        Time = st.ElapsedMilliseconds;
        VisitedStates = BFSLogic.visited.Count;
        Moves = tilemapManager.Moves;

        NextButton.gameObject.SetActive(true);
        RepeatButton.gameObject.SetActive(true);
    }

    public void UCS()
    {
        Debug.Log("UCS started...");
        Stopwatch st = new Stopwatch();
        st.Start();
        finalState = UCSLogic.UCS();
        st.Stop();

        if (finalState != null)
            tilemapManager.printPath(finalState);

        Time = st.ElapsedMilliseconds;
        VisitedStates = UCSLogic.visited.Count;
        Moves = tilemapManager.Moves;


        NextButton.gameObject.SetActive(true);
        RepeatButton.gameObject.SetActive(true);
    }

     
    public void AStar()
    {
        Debug.Log("A* started...");
        Stopwatch st = new Stopwatch();
        st.Start();
        finalState = AStarLogic.ASR();
        st.Stop();

        if (finalState != null)
            tilemapManager.printPath(finalState);

        Time = st.ElapsedMilliseconds;
        VisitedStates = AStarLogic.visited.Count;
        Moves = tilemapManager.Moves;

        NextButton.gameObject.SetActive(true);
        RepeatButton.gameObject.SetActive(true);
    }
    public void AStar2()
    {
        Debug.Log("A* started...");
        Stopwatch st = new Stopwatch();
        st.Start();
        finalState = AStarLogic.ASR2();
        st.Stop();

        if (finalState != null)
            tilemapManager.printPath(finalState);

        Time = st.ElapsedMilliseconds;
        VisitedStates = AStarLogic.visited.Count;
        Moves = tilemapManager.Moves;

        NextButton.gameObject.SetActive(true);
        RepeatButton.gameObject.SetActive(true);
    }
    public void Repeat()
    {
        tilemapManager.printPath(finalState);
    }
    public void MoveToScoreScene()
    {
        PlayerPrefs.SetInt("Moves", Moves);
        PlayerPrefs.SetFloat("Time", Time);
        PlayerPrefs.SetInt("VisitedStates", VisitedStates);
        SceneManager.LoadScene(2);
    }

}
