﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DFSLogic : Logic
{
    State startState;
    public List<State> visited = new List<State>();
    public State StartState { get => startState; set => startState = value; }

    public State DFS()
    {
        
      
        var stack = new Stack<State>();
        stack.Push(startState);
        

        while (stack.Count > 0)
        {

            var vertex = stack.Pop();

            if (vertex.isFinal())
            {
                visited.Add(vertex);
                return vertex;
            }
                

            if (isVisitedV1(visited,vertex))
                continue;

            visited.Add(vertex);

            foreach (State neighbor in vertex.GetNextMoves())
                if (!isVisitedV1(visited, neighbor))
                {
                    stack.Push(neighbor);
                }
        }

        return null;
    }



   
}
