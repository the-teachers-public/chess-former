﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AStarLogic : Logic
{
    State startState;
    public List<State> visited = new List<State>();

    public State StartState { get => startState; set => startState = value; }


    /*
    * For A* I have two versions each one have Heurstic method
    */

    // This is for the Height Heurstic 
    public State ASR()
    {

        Queue<State> priorityQueue = new Queue<State>();

        double costTracker = 1;
        startState.Cost = costTracker;

        priorityQueue.Enqueue(startState);


        while (priorityQueue.Count > 0)
        {
            costTracker++;
            var vertex = priorityQueue.Dequeue();

            if (vertex.isFinal())
            {
                visited.Add(vertex);
                return vertex;
            }

            if (isVisitedV1(visited, vertex))
                continue;

            visited.Add(vertex);


            foreach (State state in vertex.GetNextMoves())
            {
                if (!isVisitedV1(visited, state))
                {
                    
                    state.Cost = costTracker;
                    state.HeuristicValue = state.heursticCalV1();
                    priorityQueue.Enqueue(state);
                }

            }

            priorityQueue = SortQueueV2(priorityQueue);
        }

        return null;
    }

    // This is for the Near Heurstic 
    public State ASR2()
    {

        Queue<State> priorityQueue = new Queue<State>();

        double costTracker = 1;
        startState.Cost = costTracker;

        priorityQueue.Enqueue(startState);


        while (priorityQueue.Count > 0)
        {
            costTracker++;
            var vertex = priorityQueue.Dequeue();

            if (vertex.isFinal())
            {
                visited.Add(vertex);
                return vertex;
            }

            if (isVisitedV1(visited, vertex))
                continue;

            visited.Add(vertex);


            foreach (State state in vertex.GetNextMoves())
            {
                if (!isVisitedV1(visited, state))
                {

                    state.Cost = costTracker;
                    state.HeuristicValue = state.heursticCalV2();
                    priorityQueue.Enqueue(state);
                }

            }

            priorityQueue = SortQueueV2(priorityQueue);
        }

        return null;
    }
}
