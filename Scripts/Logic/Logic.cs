﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Logic : MonoBehaviour
{
    protected int movesCounter;
    public List<Position> pathOfPositions;
    protected List<State> pathOfStates;
    public int MovesCounter { get => movesCounter;}

    public bool isVisitedV1(List<State> Visited, State state)
    {
        foreach (State visi in Visited)
            if (visi.isEqualV1(state))
                return true;
        return false;
    }
    public bool isVisitedV2(List<State> Visited, State state)
    {
        foreach (State visi in Visited)
            if (visi.isEqualV2(state))
                return true;
        return false;
    }

    public Queue<State> SortQueue(Queue<State> Queue)
    {
        List<State> states = new List<State>(Queue);
        List<State> SortedStates = states.OrderBy(x => x.Cost).ToList<State>();
        return new Queue<State>(SortedStates);
    }

    public Queue<State> SortQueueV2(Queue<State> Queue)
    {
        List<State> states = new List<State>(Queue);
        List<State> SortedStates = states.OrderBy(x => x.Cost+x.HeuristicValue).ThenBy(x => x.HeuristicValue).ToList<State>();
        return new Queue<State>(SortedStates);
    }
}
