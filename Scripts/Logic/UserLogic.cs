﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

public class UserLogic : Logic
{
    TilemapManager tilemapManager;
    public List<Position> possibaleMoves;
    State S;
    bool firstRun;
    public float time=0;
    private Button next;
    private Button repeat;
    public State finalState;
    public TilemapManager TilemapManager {  set => tilemapManager = value; }
    public State S1 { set => S = value; }
    public Button Next { get => next; set => next = value; }
    public Button Repeat { get => repeat; set => repeat = value; }


    // Start is called before the first frame update
    void Awake()
    {
        pathOfPositions = new List<Position>();
        Stopwatch st = new Stopwatch();
        firstRun = true;
    }
    private void Start()
    {
        possibaleMoves = S.CheckMoves();
        tilemapManager.printValidMoves(possibaleMoves);
         
    }
    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        if (firstRun)
        {
            Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (Input.GetMouseButtonDown(0))
            {
                Vector2 checkIfValid = tilemapManager.returnPosition(mousePosition, possibaleMoves);
                if (checkIfValid.x != -1 && checkIfValid.y != -1)
                {
                    movesCounter++;
                    pathOfPositions.Add(new Position(9 - (int)checkIfValid.y, (int)checkIfValid.x));
                    S.Move(new Position(9 - (int)checkIfValid.y, (int)checkIfValid.x));
                    possibaleMoves = S.CheckMoves();
                    tilemapManager.MoveRook(new Position((int)S.CurnnetPosition.y, 9 - (int)S.CurnnetPosition.x));
                    tilemapManager.printValidMoves(possibaleMoves);
                }

            }
           
            if (S.isFinal())
            {
                main.Moves = movesCounter;
                main.Time = (long)time*1000;
                main.VisitedStates = pathOfPositions.Count;
                Next.gameObject.SetActive(true);
                repeat.gameObject.SetActive(true);
                firstRun = false;
                finalState = S;
                this.enabled = false;
            }
        }

       
    }
}
