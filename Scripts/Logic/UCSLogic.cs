﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class UCSLogic : Logic
{
    State startState;
    public List<State> visited = new List<State>();

    public State StartState { get => startState; set => startState = value; }

    public State UCS()
    {
       
        Queue<State> priorityQueue = new Queue<State>();

        double costTracker = 1;
        startState.Cost = costTracker;

        priorityQueue.Enqueue(startState);
       

        while (priorityQueue.Count > 0)
        {
            costTracker++;
            var vertex = priorityQueue.Dequeue();

            if (vertex.isFinal())
            {
                visited.Add(vertex);
                return vertex;
            }

            if (isVisitedV1(visited, vertex))
                continue;

            visited.Add(vertex);

            
            foreach (State state in vertex.GetNextMoves())
            {
                if (!isVisitedV1(visited, state))
                {
                    state.Cost = costTracker;
                    priorityQueue.Enqueue(state);                    
                }
                
            }

            priorityQueue = SortQueue(priorityQueue);
        }

        return null;
    }
}
