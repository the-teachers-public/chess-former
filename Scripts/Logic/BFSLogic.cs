﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BFSLogic : Logic
{
    State startState;
    public List<State> visited = new List<State>();
 
    public State StartState { get => startState; set => startState = value; }
   
 
    public State BFS()
    {
      
        var queue = new Queue<State>();
        queue.Enqueue(startState);

        while (queue.Count > 0)
        {

            var vertex = queue.Dequeue();

            if (vertex.isFinal())
            {
                visited.Add(vertex);
                return vertex;
            }

           

            if (isVisitedV1(visited, vertex))
                 continue;

            visited.Add(vertex);

            foreach (State neighbor in vertex.GetNextMoves())
                if (!isVisitedV1(visited, neighbor)) 
                {
                    queue.Enqueue(neighbor);
                }

           
        }
        return null;
    }
}
