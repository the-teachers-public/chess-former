﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TilemapManager : MonoBehaviour
{
    
    [SerializeField] public Tile Ground;
    [SerializeField] public Tile ValidMove;
    [SerializeField] public GameObject Rook;
    [SerializeField] public Tilemap dynamicTM;
    [SerializeField] public GameObject Enemy_King;
    [SerializeField] public Tilemap suggestionsTilemap;
    public int Moves;
    public bool stillGoing;

    public void create(int[,] array)
    {
        for(int i = 0; i < 10; i++)
        {
            for(int j = 0; j < 16; j++)
            {
                //Instantiate(Ground, new Vector3(i, j, 0), Quaternion.identity);
                if(array[i,j]==1)
                dynamicTM.SetTile(new Vector3Int(j,9-i, 0),Ground);
                else if (array[i, j] == 2)
                   Rook= Instantiate(Rook, new Vector3(j+0.5f,(9-i)+0.5f, 0), Quaternion.identity);
                else if (array[i, j] == 3)
                    Instantiate(Enemy_King, new Vector3(j + 0.5f, (9 - i) + 0.5f, 0), Quaternion.identity);

            }
        }
    }

    public void MoveRook(Position newPosition)
    {
        Vector3 kr = dynamicTM.CellToWorld(new Vector3Int(newPosition.x, newPosition.y, 0));
        Rook.transform.position = new Vector3(kr.x+0.5f, kr.y+0.5f, 0);
    }


    public void printValidMoves(List<Position> validPositions)
    {
        suggestionsTilemap.ClearAllTiles();
        foreach (Position position in validPositions)
        {
            suggestionsTilemap.SetTile(new Vector3Int(position.y, 9 - position.x, 0), ValidMove);
        }
    }

    public Vector2 returnPosition(Vector3 mousePosition,List<Position> listOfPossibolMoves)
    {
        Vector3 kr = dynamicTM.WorldToCell(mousePosition);
        foreach(Position position in listOfPossibolMoves)
        {
            if (position.x == (9-kr.y) && position.y == (kr.x))
                return new Vector2(kr.x,kr.y);
        }

        return new Vector2(-1,-1);
    }

    public  void  printPath(State finalState)
    {
        if (!stillGoing)
            StartCoroutine(WaitAndMove(1.0f, finalState));
        
        
    }
    private IEnumerator WaitAndMove(float waitTime, State finalState)
    {
        stillGoing = true;
        //TO make the states Sorted
        Stack<State> PathCollector = new Stack<State>();
        while (finalState != null)
        {
            PathCollector.Push(finalState);
            finalState = finalState.ParentState;
        }

        //Moves
        Moves = PathCollector.Count;
        Moves -= 1;

        //Make Moves with Delay
          while (PathCollector.Count>0)
        {
            State stateOfPath = PathCollector.Pop();
            MoveRook(new Position((int)stateOfPath.CurnnetPosition.y, 9 - (int)stateOfPath.CurnnetPosition.x));
            yield return new WaitForSeconds(waitTime);
        }
        stillGoing = false;
    }

    public void printVisites(List<State> visited)
    {
        foreach (var visit in visited)
        {
         //   Debug.Log("Position X: " + visit.CurnnetPosition.x + " Y: " + visit.CurnnetPosition.y +" C="+visit.Cost+ " h= " + visit.HeuristicValue);
            Debug.Log(visit.PrintState());
        }

    }
    

}
