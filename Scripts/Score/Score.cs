﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Score : MonoBehaviour
{
    [SerializeField] public TextMeshProUGUI Moves;
    [SerializeField] public TextMeshProUGUI VisitedStates;
    [SerializeField] public TextMeshProUGUI TimeTaken;

    public int moves;
    public int visitedStates;
    public float timeTaken;

    void Start()
    {
        moves = PlayerPrefs.GetInt("Moves");
        visitedStates = PlayerPrefs.GetInt("VisitedStates");
        timeTaken = PlayerPrefs.GetFloat("Time");


        Moves.SetText("Moves= " + moves);
        VisitedStates.SetText("Visited States= " + visitedStates);
        TimeTaken.SetText("Time Taken= " + timeTaken+" ms");
    }

    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene(0);
    }

   public void doExitGame()
    {
        Application.Quit();
    }
}
