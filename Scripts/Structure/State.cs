﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class State 
{
    int[,] stateArray = new int[10, 16]; //for each state has an array represnt the current rook position
    public Position finalPosition; //each state have knowledg of the final position to test if the game has ended
    Position startingPosition; //the position the rook starts  
    Position curnnetPosition; //the current position of the Rook
    protected State parentState; //which represnt the previous State
    protected bool hasParent; //value that tells me if the state has a Parent State (helps me to print the final Path)
    Double cost=10000; 
    Double heuristicValue; 

    /*
    * Set and Get
    */
    public int[,] StateArray { get => stateArray; }
    public Position CurnnetPosition { get => curnnetPosition; }
    public State ParentState { get => parentState; set => parentState = value; }
    public double Cost { get => cost; set => cost = value; }
    protected bool HasParent { get => hasParent; set => hasParent = value; }
    public double HeuristicValue { get => heuristicValue; set => heuristicValue = value; }


    public State(int[,] stateArray, Position startingPosition,Position finalPosition)
    {
        this.stateArray = stateArray;
        this.startingPosition = startingPosition;
        this.finalPosition = finalPosition;
        this.curnnetPosition = startingPosition;
        this.parentState = null;
        this.hasParent = false;
    }


    public State(int[,] stateArray, Position startingPosition, Position finalPosition, bool hasParent,State parentState)
    {
        this.stateArray = stateArray;
        this.startingPosition = startingPosition;
        this.finalPosition = finalPosition;
        this.curnnetPosition = startingPosition;
        this.parentState = parentState;
        this.hasParent = hasParent;
    }

    public List<Position> CheckMoves()
    {
        List<Position> listOfPositions=new List<Position>();
        int x = curnnetPosition.x;
        int y = curnnetPosition.y;

        //Note: checking for up&down for now is not useful in the future
        //      if we added a floating platform we can use them

        //check up moves
        //listOfPositions.AddRange(CheckMoveUp(x - 1, y));

        //check down moves
        //listOfPositions.AddRange(CheckMoveDown(x + 1, y));

        //check right moves
        listOfPositions.AddRange(CheckMoveRight(x, y + 1));

        //check left moves
        listOfPositions.AddRange(CheckMoveLeft(x, y - 1));
       
        return listOfPositions;
    }

    public void Move(Position newPosition)
    {
        stateArray[curnnetPosition.x, curnnetPosition.y] = 0;
        curnnetPosition=new Position(newPosition.x,newPosition.y);
        while (curnnetPosition.x + 1 < 10&&stateArray[curnnetPosition.x + 1, curnnetPosition.y] != 1)
        {
            curnnetPosition.x+=1;
        }
        stateArray[curnnetPosition.x, curnnetPosition.y] = 2;
    }

    public List<State> GetNextMoves()
    {
        List<State> Next_States = new List<State>();
        List<Position>Possible_Positions = CheckMoves();
        foreach(Position possible_position in Possible_Positions) {
            State S = DeepCopy();
            S.Move(possible_position);
            State newState = new State(S.stateArray, S.curnnetPosition, S.finalPosition, true, this);
            Next_States.Add(newState);
        }

        return Next_States;
    }

    public bool isFinal()
    {
        return curnnetPosition.x == finalPosition.x && curnnetPosition.y == finalPosition.y ? true : false;
    }

    public String PrintState()
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < stateArray.GetLength(0); i++)
        {
            for (int j = 0; j < stateArray.GetLength(1); j++)
            {
                sb.Append(stateArray[i, j]);
                sb.Append(' ');
            }
            sb.AppendLine();
        }
        return (sb.ToString());
    }

    public bool isEqualV1(State state)
    {
       return curnnetPosition.x == state.curnnetPosition.x && curnnetPosition.y == state.curnnetPosition.y ? true : false;
    }


    public bool isEqualV2(State state)
    {

        State temp = this;


        //check if this state has not been visited before.
        if (temp.isEqualV1(state)==false)
            return false;

        //Parents check:
        //until now I know this state has been visited before.
        //check if it has the same parents…
        //if so we don’t need to enter it again => it has the same path

        if (temp.hasParent && state.hasParent && temp.parentState.isEqualV1(state.parentState))
            return true;

        //Loop cutting:
        //if the state is a (Parent) to any state cut it.
        //Note: I only add the state to visited after I remove it from (Stack or Queue).
        //      So, I can generate multiple Paths to the same state

        while (temp.hasParent)
        {
            temp = temp.parentState.DeepCopy();

            if (temp.isEqualV1(state))
                return true;
        }

        //now I know for sure this state has a different path and doesn’t cause a Loop.
        return false;
    }

    //Height Heurstic
    public double heursticCalV1()
    {

        //height diff
        double dist = (finalPosition.x - curnnetPosition.x);

        //if the Rook moved under the King its unUsefull state so make it unImportant
        if (dist < 0)
            return 99;

        return dist;
    }

    //Near heurstic
    public double heursticCalV2()
    {
        //Near diff
        //where the Rook will go the Nearest Position he can find

        double dist = (finalPosition.x - curnnetPosition.x);
        //if the Rook moved under the King its unUsefull state so make it un important
        if (dist < 0)
            return 99;

        //we need the Abs value of this value to see what is the 
        // Distance in Y axis 
        // and the "+1" to make it on King Position not behind a step
        dist += Math.Abs(finalPosition.y - curnnetPosition.y)+1;
        if (dist == 0)
            return 0;
        return dist;
    }
   
    State DeepCopy()
    {
        return new State((int[,])stateArray.Clone(), curnnetPosition, finalPosition,hasParent,ParentState);
    }

    List<Position> CheckMoveUp(int x,int y)
    {
        List<Position> list = new List<Position>();
        while (x >= 0)
        {
            //if there is wall 
            if (stateArray[x, y] == 1||x<0)
            {
                break;
            }
            list.Add(new Position(x, y));
            x--;
        }
        return list;
    }
    List<Position> CheckMoveDown(int x, int y)
    {
        List<Position> list = new List<Position>();
        while (x < 10)
        {
            //if there is wall 
            if (stateArray[x, y] == 1)
            {
                break;
            }
            list.Add(new Position(x, y));
            x++;
        }
        return list;
    }
    List<Position> CheckMoveRight(int x, int y)
    {
        
        List<Position> list = new List<Position>();
        while (y < 16)
        {
            //if there is wall 
            if (stateArray[x, y] == 1)
            {
                break;
            }
            list.Add(new Position(x, y));
            y++;
        }
        return list;
    }
    List<Position> CheckMoveLeft(int x, int y)
    {

        List<Position> list = new List<Position>();
        while (y >= 0)
        {
            //if there is wall 
            if (stateArray[x, y] == 1)
            {
                break;
            }
            list.Add(new Position(x, y));
            y--;
        }
        return list;
    }

}
