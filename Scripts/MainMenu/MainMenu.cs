﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using AnotherFileBrowser.Windows;
using UnityEngine.Networking;
using UnityEngine.UI;
public class MainMenu : MonoBehaviour
{
    private int[,] startState = new int[10, 16];
    private Position startPosition;
    private Position finalPosition;
    protected string Path;
    public static MainMenuManger mainMenuManger;
    [SerializeField] Button playButton;
    [SerializeField] Button DFSButton;
    [SerializeField] Button BFSButton;
    [SerializeField] Button UCSButton;
    [SerializeField] Button ASTARButton;
    [SerializeField] Button SelectButton;
    [SerializeField] Button ContinueButton;
    [SerializeField] Button H1;
    [SerializeField] Button H2;
    public int[,] StartState { get => startState; set => startState = value; }
    public Position StartPosition { get => startPosition; set => startPosition = value; }
    public Position FinalPosition { get => finalPosition; set => finalPosition = value; }

    private void Awake()
    {
        if(MainMenu.mainMenuManger==null)
        mainMenuManger = gameObject.AddComponent<MainMenuManger>();
        if (!mainMenuManger.Init)
        {
            DontDestroyOnLoad(mainMenuManger.gameObject);
        }
           
    }
    public void PlayerInput()
    {
        PlayerPrefs.SetInt("choice", 1);
        SceneManager.LoadScene(1);

    }

    public void DFS()
    {
      PlayerPrefs.SetInt("choice", 2);
      SceneManager.LoadScene(1);
    }

    public void BFS()
    {
        PlayerPrefs.SetInt("choice", 3);
        SceneManager.LoadScene(1);
    }

    public void UCS()
    {
        PlayerPrefs.SetInt("choice", 4);
        SceneManager.LoadScene(1);
    }

    public void AstarH1()
    {
        PlayerPrefs.SetInt("choice", 5);
        SceneManager.LoadScene(1);
    }
    public void AstarH2()
    {
        PlayerPrefs.SetInt("choice", 6);
        SceneManager.LoadScene(1);
    }

    public void AstarSelected()
    {
        playButton.gameObject.SetActive(false);
        DFSButton.gameObject.SetActive(false);
        BFSButton.gameObject.SetActive(false);
        UCSButton.gameObject.SetActive(false);
        ASTARButton.gameObject.SetActive(false);
        H1.gameObject.SetActive(true);
        H2.gameObject.SetActive(true);
    }
    public void ReadFile()
    {
        StreamReader file = new StreamReader(Path);
        string line;
        
        for(int i = 0; i < 10; i++)
        {
            line = file.ReadLine();
            for (int j = 0; j < 16; j++)
            {
                if (line[j] == '0')
                    StartState[i, j] = 0;
                else if (line[j] == '1')
                    StartState[i, j] = 1;
                else if (line[j] == '2')
                {
                    StartState[i, j] = 2;
                    mainMenuManger.StartPostion = new Position(i, j);
                }
                   
                else if (line[j] == '3')
                {
                    StartState[i, j] = 3;
                    mainMenuManger.FinalPosition = new Position(i, j);
                }
                else
                    continue;

            }
        }
        file.Close();
        mainMenuManger.StartState = StartState;
        mainMenuManger.Init = true;
        SelectButton.gameObject.SetActive(false);
        ContinueButton.gameObject.SetActive(false);
        playButton.gameObject.SetActive(true); 
        DFSButton.gameObject.SetActive(true);
        BFSButton.gameObject.SetActive(true);
        UCSButton.gameObject.SetActive(true);
        ASTARButton.gameObject.SetActive(true);

        //for (int i = 0; i < 10; i++)
        //{
        //    for (int j = 0; j < 16; j++)
        //    {
        //        Debug.Log(StartState[i, j] + " ");
        //    }
        //    Debug.Log("\n");
        //}

    }

    public void OpenFileBrowser()
    {
        //if(mainMenu)
        //Destroy(mainMenu);
        //DontDestroyOnLoad(this.gameObject);
        var bp = new BrowserProperties();
        bp.filter = "Text files (*.txt*)|*.txt*";
        bp.filterIndex = 0;

        new FileBrowser().OpenFileBrowser(bp , path =>
        {
            if (path != null)
                this.Path = path;
            else
                Debug.LogWarning("No file Selected");
        });

        if(Path!=null)
        ReadFile();
    }

    public void Continue()
    {
        if (mainMenuManger.Init) 
        {
            SelectButton.gameObject.SetActive(false);
            ContinueButton.gameObject.SetActive(false);
            playButton.gameObject.SetActive(true);
            DFSButton.gameObject.SetActive(true);
            BFSButton.gameObject.SetActive(true);
            UCSButton.gameObject.SetActive(true);
            ASTARButton.gameObject.SetActive(true);
        }
       
    }

   
}
