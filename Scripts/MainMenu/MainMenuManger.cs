﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManger : MonoBehaviour
{
    private int[,] startState = new int[10, 16];
    private Position finalPosition;
    private Position startPostion;
    private bool init;
    public int[,] StartState { get => startState; set => startState = value; }
    public Position FinalPosition { get => finalPosition; set => finalPosition = value; }
    public Position StartPostion { get => startPostion; set => startPostion = value; }
    public bool Init { get => init; set => init = value; }

    //private void Awake()
    //{
    //    if (MainMenu.mainMenuManger != this)
    //        Destroy(this);
    //}
    public void SetStartingState(int[,] startState, Position startPostion, Position finalPosition)
    {
        this.StartState = startState;
        this.StartPostion = startPostion;
        this.FinalPosition = finalPosition;
    }
}
