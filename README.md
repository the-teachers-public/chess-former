# Chess Former

## Table of Contents

- [Introduction](#introduction)
- [Game Description](#game-description)
- [Algorithms](#algorithms)
- [Contributing](#contributing)
- [License](#license)

---

## Introduction

Welcome to the Chess Former! This repository serves as a resource for students and developers interested in creating chess-based puzzle games with pathfinding algorithms. In this project, we implement Depth-First Search (DFS), Breadth-First Search (BFS), Uniform Cost Search (UCS), and A* algorithms to find solutions for puzzle boards. This README will guide you through the project and help you understand its structure.

---

## Game Description

The Chess Former is a puzzle board game similar to chess. In this game, chess pieces have specific movement rules. For example, a rook can only move left, right, up, and down, just like in chess. Your goal is to design puzzles that challenge players to navigate these movement constraints and solve the puzzles using the provided algorithms.

---

## Algorithms

### Depth-First Search (DFS)

- DFS is a graph traversal algorithm.
- It explores as far as possible along each branch before backtracking.

### Breadth-First Search (BFS)

- BFS explores all the neighbor nodes at the current depth before moving to the next level.

### Uniform Cost Search (UCS)

- UCS is a variant of Dijkstra's algorithm that takes into account the cost to reach each node.

### A* Algorithm

- A* is an informed search algorithm that combines the benefits of BFS and UCS while using a heuristic to guide the search efficiently.

---

## Contributing

If you'd like to contribute to this project, please follow these steps:

1. Fork this repository.
2. Create a new branch for your feature or bug fix.
3. Make your changes and commit them.
4. Create a pull request, explaining your changes and their benefits.

---

## License

This project is licensed under the [MIT License](LICENSE). You are free to use, modify, and distribute this project as per the terms of the license.

---
