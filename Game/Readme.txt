Welcome to the Chess Former (Game)!
---------------------------------------------------------------
Notes:
- Level selection:
  You can now choose between two levels that are already written.
  Please note that there is a level folder where you can find these levels.

- Cell types:
  Each cell on the board is represented by a number.
  "0" represents an empty cell.
  "1" represents a ground cell.
  "2" represents a Rook cell.
  "3" represents a King cell.

- Design your own level:
  You can create your own level by following this syntax.
  Please note that you can only define one Rook and one King.

Enjoy...